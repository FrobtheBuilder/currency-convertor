fx = require "money"

populateCurrencies = ->
    $.getJSON "http://api.fixer.io/latest?callback=?", (data) ->
        for k, v of data.rates
            $("<option></option>").text(k).appendTo(".in-currency")
            $("<option></option>").text(k).appendTo(".out-currency")
        

displayResults = (data) ->
    fx.rates = data.rates
    amt = $(".in").val()
    unless amt is "" or isNaN amt
        b = $(".in-currency").find(":selected").text()
        to = $(".out-currency").find(":selected").text()
        $(".out").text "#{amt} #{b} = " + fx(amt).from(b).to(to).toFixed(2) + " " + to
        $(".out-rate").text "1 #{b} = " + fx(1).from(b).to(to).toFixed(2) + " " + to
    else
        $(".out").text "= Invalid Input."
        $(".out-rate").text "Please retry."

registerHandlers = ->
    $(".submit").click (e) ->
        $.getJSON "http://api.fixer.io/latest?callback=?", displayResults
    

$ ->
    populateCurrencies()
    registerHandlers()
