(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var displayResults, fx, populateCurrencies, registerHandlers;

fx = require("money");

populateCurrencies = function() {
  return $.getJSON("http://api.fixer.io/latest?callback=?", function(data) {
    var k, ref, results, v;
    ref = data.rates;
    results = [];
    for (k in ref) {
      v = ref[k];
      $("<option></option>").text(k).appendTo(".in-currency");
      results.push($("<option></option>").text(k).appendTo(".out-currency"));
    }
    return results;
  });
};

displayResults = function(data) {
  var amt, b, to;
  fx.rates = data.rates;
  amt = $(".in").val();
  if (!(amt === "" || isNaN(amt))) {
    b = $(".in-currency").find(":selected").text();
    to = $(".out-currency").find(":selected").text();
    $(".out").text((amt + " " + b + " = ") + fx(amt).from(b).to(to).toFixed(2) + " " + to);
    return $(".out-rate").text(("1 " + b + " = ") + fx(1).from(b).to(to).toFixed(2) + " " + to);
  } else {
    $(".out").text("= Invalid Input.");
    return $(".out-rate").text("Please retry.");
  }
};

registerHandlers = function() {
  return $(".submit").click(function(e) {
    return $.getJSON("http://api.fixer.io/latest?callback=?", displayResults);
  });
};

$(function() {
  populateCurrencies();
  return registerHandlers();
});


},{"money":2}],2:[function(require,module,exports){
/*!
 * money.js / fx() v0.2
 * Copyright 2014 Open Exchange Rates
 *
 * JavaScript library for realtime currency conversion and exchange rate calculation.
 *
 * Freely distributable under the MIT license.
 * Portions of money.js are inspired by or borrowed from underscore.js
 *
 * For details, examples and documentation:
 * http://openexchangerates.github.io/money.js/
 */
(function(root, undefined) {

	// Create a safe reference to the money.js object for use below.
	var fx = function(obj) {
		return new fxWrapper(obj);
	};

	// Current version.
	fx.version = '0.2';


	/* --- Setup --- */

	// fxSetup can be defined before loading money.js, to set the exchange rates and the base
	// (and default from/to) currencies - or the rates can be loaded in later if needed.
	var fxSetup = root.fxSetup || {
		rates : {},
		base : ""
	};

	// Object containing exchange rates relative to the fx.base currency, eg { "GBP" : "0.64" }
	fx.rates = fxSetup.rates;

	// Default exchange rate base currency (eg "USD"), which all the exchange rates are relative to
	fx.base = fxSetup.base;

	// Default from / to currencies for conversion via fx.convert():
	fx.settings = {
		from : fxSetup.from || fx.base,
		to : fxSetup.to || fx.base
	};


	/* --- Conversion --- */

	// The base function of the library: converts a value from one currency to another
	var convert = fx.convert = function(val, opts) {
		// Convert arrays recursively
		if (typeof val === 'object' && val.length) {
			for (var i = 0; i< val.length; i++ ) {
				val[i] = convert(val[i], opts);
			}
			return val;
		}

		// Make sure we gots some opts
		opts = opts || {};

		// We need to know the `from` and `to` currencies
		if( !opts.from ) opts.from = fx.settings.from;
		if( !opts.to ) opts.to = fx.settings.to;

		// Multiple the value by the exchange rate
		return val * getRate( opts.to, opts.from );
	};

	// Returns the exchange rate to `target` currency from `base` currency
	var getRate = function(to, from) {
		// Save bytes in minified version
		var rates = fx.rates;

		// Make sure the base rate is in the rates object:
		rates[fx.base] = 1;

		// Throw an error if either rate isn't in the rates array
		if ( !rates[to] || !rates[from] ) throw "fx error";

		// If `from` currency === fx.base, return the basic exchange rate for the `to` currency
		if ( from === fx.base ) {
			return rates[to];
		}

		// If `to` currency === fx.base, return the basic inverse rate of the `from` currency
		if ( to === fx.base ) {
			return 1 / rates[from];
		}

		// Otherwise, return the `to` rate multipled by the inverse of the `from` rate to get the
		// relative exchange rate between the two currencies
		return rates[to] * (1 / rates[from]);
	};


	/* --- OOP wrapper and chaining --- */

	// If fx(val) is called as a function, it returns a wrapped object that can be used OO-style
	var fxWrapper = function(val) {
		// Experimental: parse strings to pull out currency code and value:
		if ( typeof	val === "string" ) {
			this._v = parseFloat(val.replace(/[^0-9-.]/g, ""));
			this._fx = val.replace(/([^A-Za-z])/g, "");
		} else {
			this._v = val;
		}
	};

	// Expose `wrapper.prototype` as `fx.prototype`
	var fxProto = fx.prototype = fxWrapper.prototype;

	// fx(val).convert(opts) does the same thing as fx.convert(val, opts)
	fxProto.convert = function() {
		var args = Array.prototype.slice.call(arguments);
		args.unshift(this._v);
		return convert.apply(fx, args);
	};

	// fx(val).from(currency) returns a wrapped `fx` where the value has been converted from
	// `currency` to the `fx.base` currency. Should be followed by `.to(otherCurrency)`
	fxProto.from = function(currency) {
		var wrapped = fx(convert(this._v, {from: currency, to: fx.base}));
		wrapped._fx = fx.base;
		return wrapped;
	};

	// fx(val).to(currency) returns the value, converted from `fx.base` to `currency`
	fxProto.to = function(currency) {
		return convert(this._v, {from: this._fx ? this._fx : fx.settings.from, to: currency});
	};


	/* --- Module Definition --- */

	// Export the fx object for CommonJS. If being loaded as an AMD module, define it as such.
	// Otherwise, just add `fx` to the global object
	if (typeof exports !== 'undefined') {
		if (typeof module !== 'undefined' && module.exports) {
			exports = module.exports = fx;
		}
		exports.fx = fx;
	} else if (typeof define === 'function' && define.amd) {
		// Return the library as an AMD module:
		define([], function() {
			return fx;
		});
	} else {
		// Use fx.noConflict to restore `fx` back to its original value before money.js loaded.
		// Returns a reference to the library's `fx` object; e.g. `var money = fx.noConflict();`
		fx.noConflict = (function(previousFx) {
			return function() {
				// Reset the value of the root's `fx` variable:
				root.fx = previousFx;
				// Delete the noConflict function:
				fx.noConflict = undefined;
				// Return reference to the library to re-assign it:
				return fx;
			};
		})(root.fx);

		// Declare `fx` on the root (global/window) object:
		root['fx'] = fx;
	}

	// Root will be `window` in browser or `global` on the server:
}(this));

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJDOlxcVXNlcnNcXEZyb2JcXERyb3Bib3hcXHByb2plY3RzXFxjdXJyZW5jeS1jb252ZXJ0b3JcXGFwcFxcY29mZmVlXFxtYWluLmNvZmZlZSIsIm5vZGVfbW9kdWxlcy9tb25leS9tb25leS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBLElBQUE7O0FBQUEsRUFBQSxHQUFLLE9BQUEsQ0FBUSxPQUFSOztBQUVMLGtCQUFBLEdBQXFCLFNBQUE7U0FDakIsQ0FBQyxDQUFDLE9BQUYsQ0FBVSx1Q0FBVixFQUFtRCxTQUFDLElBQUQ7QUFDL0MsUUFBQTtBQUFBO0FBQUE7U0FBQSxRQUFBOztNQUNJLENBQUEsQ0FBRSxtQkFBRixDQUFzQixDQUFDLElBQXZCLENBQTRCLENBQTVCLENBQThCLENBQUMsUUFBL0IsQ0FBd0MsY0FBeEM7bUJBQ0EsQ0FBQSxDQUFFLG1CQUFGLENBQXNCLENBQUMsSUFBdkIsQ0FBNEIsQ0FBNUIsQ0FBOEIsQ0FBQyxRQUEvQixDQUF3QyxlQUF4QztBQUZKOztFQUQrQyxDQUFuRDtBQURpQjs7QUFPckIsY0FBQSxHQUFpQixTQUFDLElBQUQ7QUFDYixNQUFBO0VBQUEsRUFBRSxDQUFDLEtBQUgsR0FBVyxJQUFJLENBQUM7RUFDaEIsR0FBQSxHQUFNLENBQUEsQ0FBRSxLQUFGLENBQVEsQ0FBQyxHQUFULENBQUE7RUFDTixJQUFBLENBQUEsQ0FBTyxHQUFBLEtBQU8sRUFBUCxJQUFhLEtBQUEsQ0FBTSxHQUFOLENBQXBCLENBQUE7SUFDSSxDQUFBLEdBQUksQ0FBQSxDQUFFLGNBQUYsQ0FBaUIsQ0FBQyxJQUFsQixDQUF1QixXQUF2QixDQUFtQyxDQUFDLElBQXBDLENBQUE7SUFDSixFQUFBLEdBQUssQ0FBQSxDQUFFLGVBQUYsQ0FBa0IsQ0FBQyxJQUFuQixDQUF3QixXQUF4QixDQUFvQyxDQUFDLElBQXJDLENBQUE7SUFDTCxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsSUFBVixDQUFlLENBQUcsR0FBRCxHQUFLLEdBQUwsR0FBUSxDQUFSLEdBQVUsS0FBWixDQUFBLEdBQW1CLEVBQUEsQ0FBRyxHQUFILENBQU8sQ0FBQyxJQUFSLENBQWEsQ0FBYixDQUFlLENBQUMsRUFBaEIsQ0FBbUIsRUFBbkIsQ0FBc0IsQ0FBQyxPQUF2QixDQUErQixDQUEvQixDQUFuQixHQUF1RCxHQUF2RCxHQUE2RCxFQUE1RTtXQUNBLENBQUEsQ0FBRSxXQUFGLENBQWMsQ0FBQyxJQUFmLENBQW9CLENBQUEsSUFBQSxHQUFLLENBQUwsR0FBTyxLQUFQLENBQUEsR0FBYyxFQUFBLENBQUcsQ0FBSCxDQUFLLENBQUMsSUFBTixDQUFXLENBQVgsQ0FBYSxDQUFDLEVBQWQsQ0FBaUIsRUFBakIsQ0FBb0IsQ0FBQyxPQUFyQixDQUE2QixDQUE3QixDQUFkLEdBQWdELEdBQWhELEdBQXNELEVBQTFFLEVBSko7R0FBQSxNQUFBO0lBTUksQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLElBQVYsQ0FBZSxrQkFBZjtXQUNBLENBQUEsQ0FBRSxXQUFGLENBQWMsQ0FBQyxJQUFmLENBQW9CLGVBQXBCLEVBUEo7O0FBSGE7O0FBWWpCLGdCQUFBLEdBQW1CLFNBQUE7U0FDZixDQUFBLENBQUUsU0FBRixDQUFZLENBQUMsS0FBYixDQUFtQixTQUFDLENBQUQ7V0FDZixDQUFDLENBQUMsT0FBRixDQUFVLHVDQUFWLEVBQW1ELGNBQW5EO0VBRGUsQ0FBbkI7QUFEZTs7QUFLbkIsQ0FBQSxDQUFFLFNBQUE7RUFDRSxrQkFBQSxDQUFBO1NBQ0EsZ0JBQUEsQ0FBQTtBQUZGLENBQUY7Ozs7QUMxQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJmeCA9IHJlcXVpcmUgXCJtb25leVwiXHJcblxyXG5wb3B1bGF0ZUN1cnJlbmNpZXMgPSAtPlxyXG4gICAgJC5nZXRKU09OIFwiaHR0cDovL2FwaS5maXhlci5pby9sYXRlc3Q/Y2FsbGJhY2s9P1wiLCAoZGF0YSkgLT5cclxuICAgICAgICBmb3IgaywgdiBvZiBkYXRhLnJhdGVzXHJcbiAgICAgICAgICAgICQoXCI8b3B0aW9uPjwvb3B0aW9uPlwiKS50ZXh0KGspLmFwcGVuZFRvKFwiLmluLWN1cnJlbmN5XCIpXHJcbiAgICAgICAgICAgICQoXCI8b3B0aW9uPjwvb3B0aW9uPlwiKS50ZXh0KGspLmFwcGVuZFRvKFwiLm91dC1jdXJyZW5jeVwiKVxyXG4gICAgICAgIFxyXG5cclxuZGlzcGxheVJlc3VsdHMgPSAoZGF0YSkgLT5cclxuICAgIGZ4LnJhdGVzID0gZGF0YS5yYXRlc1xyXG4gICAgYW10ID0gJChcIi5pblwiKS52YWwoKVxyXG4gICAgdW5sZXNzIGFtdCBpcyBcIlwiIG9yIGlzTmFOIGFtdFxyXG4gICAgICAgIGIgPSAkKFwiLmluLWN1cnJlbmN5XCIpLmZpbmQoXCI6c2VsZWN0ZWRcIikudGV4dCgpXHJcbiAgICAgICAgdG8gPSAkKFwiLm91dC1jdXJyZW5jeVwiKS5maW5kKFwiOnNlbGVjdGVkXCIpLnRleHQoKVxyXG4gICAgICAgICQoXCIub3V0XCIpLnRleHQgXCIje2FtdH0gI3tifSA9IFwiICsgZngoYW10KS5mcm9tKGIpLnRvKHRvKS50b0ZpeGVkKDIpICsgXCIgXCIgKyB0b1xyXG4gICAgICAgICQoXCIub3V0LXJhdGVcIikudGV4dCBcIjEgI3tifSA9IFwiICsgZngoMSkuZnJvbShiKS50byh0bykudG9GaXhlZCgyKSArIFwiIFwiICsgdG9cclxuICAgIGVsc2VcclxuICAgICAgICAkKFwiLm91dFwiKS50ZXh0IFwiPSBJbnZhbGlkIElucHV0LlwiXHJcbiAgICAgICAgJChcIi5vdXQtcmF0ZVwiKS50ZXh0IFwiUGxlYXNlIHJldHJ5LlwiXHJcblxyXG5yZWdpc3RlckhhbmRsZXJzID0gLT5cclxuICAgICQoXCIuc3VibWl0XCIpLmNsaWNrIChlKSAtPlxyXG4gICAgICAgICQuZ2V0SlNPTiBcImh0dHA6Ly9hcGkuZml4ZXIuaW8vbGF0ZXN0P2NhbGxiYWNrPT9cIiwgZGlzcGxheVJlc3VsdHNcclxuICAgIFxyXG5cclxuJCAtPlxyXG4gICAgcG9wdWxhdGVDdXJyZW5jaWVzKClcclxuICAgIHJlZ2lzdGVySGFuZGxlcnMoKVxyXG4iLCIvKiFcbiAqIG1vbmV5LmpzIC8gZngoKSB2MC4yXG4gKiBDb3B5cmlnaHQgMjAxNCBPcGVuIEV4Y2hhbmdlIFJhdGVzXG4gKlxuICogSmF2YVNjcmlwdCBsaWJyYXJ5IGZvciByZWFsdGltZSBjdXJyZW5jeSBjb252ZXJzaW9uIGFuZCBleGNoYW5nZSByYXRlIGNhbGN1bGF0aW9uLlxuICpcbiAqIEZyZWVseSBkaXN0cmlidXRhYmxlIHVuZGVyIHRoZSBNSVQgbGljZW5zZS5cbiAqIFBvcnRpb25zIG9mIG1vbmV5LmpzIGFyZSBpbnNwaXJlZCBieSBvciBib3Jyb3dlZCBmcm9tIHVuZGVyc2NvcmUuanNcbiAqXG4gKiBGb3IgZGV0YWlscywgZXhhbXBsZXMgYW5kIGRvY3VtZW50YXRpb246XG4gKiBodHRwOi8vb3BlbmV4Y2hhbmdlcmF0ZXMuZ2l0aHViLmlvL21vbmV5LmpzL1xuICovXG4oZnVuY3Rpb24ocm9vdCwgdW5kZWZpbmVkKSB7XG5cblx0Ly8gQ3JlYXRlIGEgc2FmZSByZWZlcmVuY2UgdG8gdGhlIG1vbmV5LmpzIG9iamVjdCBmb3IgdXNlIGJlbG93LlxuXHR2YXIgZnggPSBmdW5jdGlvbihvYmopIHtcblx0XHRyZXR1cm4gbmV3IGZ4V3JhcHBlcihvYmopO1xuXHR9O1xuXG5cdC8vIEN1cnJlbnQgdmVyc2lvbi5cblx0ZngudmVyc2lvbiA9ICcwLjInO1xuXG5cblx0LyogLS0tIFNldHVwIC0tLSAqL1xuXG5cdC8vIGZ4U2V0dXAgY2FuIGJlIGRlZmluZWQgYmVmb3JlIGxvYWRpbmcgbW9uZXkuanMsIHRvIHNldCB0aGUgZXhjaGFuZ2UgcmF0ZXMgYW5kIHRoZSBiYXNlXG5cdC8vIChhbmQgZGVmYXVsdCBmcm9tL3RvKSBjdXJyZW5jaWVzIC0gb3IgdGhlIHJhdGVzIGNhbiBiZSBsb2FkZWQgaW4gbGF0ZXIgaWYgbmVlZGVkLlxuXHR2YXIgZnhTZXR1cCA9IHJvb3QuZnhTZXR1cCB8fCB7XG5cdFx0cmF0ZXMgOiB7fSxcblx0XHRiYXNlIDogXCJcIlxuXHR9O1xuXG5cdC8vIE9iamVjdCBjb250YWluaW5nIGV4Y2hhbmdlIHJhdGVzIHJlbGF0aXZlIHRvIHRoZSBmeC5iYXNlIGN1cnJlbmN5LCBlZyB7IFwiR0JQXCIgOiBcIjAuNjRcIiB9XG5cdGZ4LnJhdGVzID0gZnhTZXR1cC5yYXRlcztcblxuXHQvLyBEZWZhdWx0IGV4Y2hhbmdlIHJhdGUgYmFzZSBjdXJyZW5jeSAoZWcgXCJVU0RcIiksIHdoaWNoIGFsbCB0aGUgZXhjaGFuZ2UgcmF0ZXMgYXJlIHJlbGF0aXZlIHRvXG5cdGZ4LmJhc2UgPSBmeFNldHVwLmJhc2U7XG5cblx0Ly8gRGVmYXVsdCBmcm9tIC8gdG8gY3VycmVuY2llcyBmb3IgY29udmVyc2lvbiB2aWEgZnguY29udmVydCgpOlxuXHRmeC5zZXR0aW5ncyA9IHtcblx0XHRmcm9tIDogZnhTZXR1cC5mcm9tIHx8IGZ4LmJhc2UsXG5cdFx0dG8gOiBmeFNldHVwLnRvIHx8IGZ4LmJhc2Vcblx0fTtcblxuXG5cdC8qIC0tLSBDb252ZXJzaW9uIC0tLSAqL1xuXG5cdC8vIFRoZSBiYXNlIGZ1bmN0aW9uIG9mIHRoZSBsaWJyYXJ5OiBjb252ZXJ0cyBhIHZhbHVlIGZyb20gb25lIGN1cnJlbmN5IHRvIGFub3RoZXJcblx0dmFyIGNvbnZlcnQgPSBmeC5jb252ZXJ0ID0gZnVuY3Rpb24odmFsLCBvcHRzKSB7XG5cdFx0Ly8gQ29udmVydCBhcnJheXMgcmVjdXJzaXZlbHlcblx0XHRpZiAodHlwZW9mIHZhbCA9PT0gJ29iamVjdCcgJiYgdmFsLmxlbmd0aCkge1xuXHRcdFx0Zm9yICh2YXIgaSA9IDA7IGk8IHZhbC5sZW5ndGg7IGkrKyApIHtcblx0XHRcdFx0dmFsW2ldID0gY29udmVydCh2YWxbaV0sIG9wdHMpO1xuXHRcdFx0fVxuXHRcdFx0cmV0dXJuIHZhbDtcblx0XHR9XG5cblx0XHQvLyBNYWtlIHN1cmUgd2UgZ290cyBzb21lIG9wdHNcblx0XHRvcHRzID0gb3B0cyB8fCB7fTtcblxuXHRcdC8vIFdlIG5lZWQgdG8ga25vdyB0aGUgYGZyb21gIGFuZCBgdG9gIGN1cnJlbmNpZXNcblx0XHRpZiggIW9wdHMuZnJvbSApIG9wdHMuZnJvbSA9IGZ4LnNldHRpbmdzLmZyb207XG5cdFx0aWYoICFvcHRzLnRvICkgb3B0cy50byA9IGZ4LnNldHRpbmdzLnRvO1xuXG5cdFx0Ly8gTXVsdGlwbGUgdGhlIHZhbHVlIGJ5IHRoZSBleGNoYW5nZSByYXRlXG5cdFx0cmV0dXJuIHZhbCAqIGdldFJhdGUoIG9wdHMudG8sIG9wdHMuZnJvbSApO1xuXHR9O1xuXG5cdC8vIFJldHVybnMgdGhlIGV4Y2hhbmdlIHJhdGUgdG8gYHRhcmdldGAgY3VycmVuY3kgZnJvbSBgYmFzZWAgY3VycmVuY3lcblx0dmFyIGdldFJhdGUgPSBmdW5jdGlvbih0bywgZnJvbSkge1xuXHRcdC8vIFNhdmUgYnl0ZXMgaW4gbWluaWZpZWQgdmVyc2lvblxuXHRcdHZhciByYXRlcyA9IGZ4LnJhdGVzO1xuXG5cdFx0Ly8gTWFrZSBzdXJlIHRoZSBiYXNlIHJhdGUgaXMgaW4gdGhlIHJhdGVzIG9iamVjdDpcblx0XHRyYXRlc1tmeC5iYXNlXSA9IDE7XG5cblx0XHQvLyBUaHJvdyBhbiBlcnJvciBpZiBlaXRoZXIgcmF0ZSBpc24ndCBpbiB0aGUgcmF0ZXMgYXJyYXlcblx0XHRpZiAoICFyYXRlc1t0b10gfHwgIXJhdGVzW2Zyb21dICkgdGhyb3cgXCJmeCBlcnJvclwiO1xuXG5cdFx0Ly8gSWYgYGZyb21gIGN1cnJlbmN5ID09PSBmeC5iYXNlLCByZXR1cm4gdGhlIGJhc2ljIGV4Y2hhbmdlIHJhdGUgZm9yIHRoZSBgdG9gIGN1cnJlbmN5XG5cdFx0aWYgKCBmcm9tID09PSBmeC5iYXNlICkge1xuXHRcdFx0cmV0dXJuIHJhdGVzW3RvXTtcblx0XHR9XG5cblx0XHQvLyBJZiBgdG9gIGN1cnJlbmN5ID09PSBmeC5iYXNlLCByZXR1cm4gdGhlIGJhc2ljIGludmVyc2UgcmF0ZSBvZiB0aGUgYGZyb21gIGN1cnJlbmN5XG5cdFx0aWYgKCB0byA9PT0gZnguYmFzZSApIHtcblx0XHRcdHJldHVybiAxIC8gcmF0ZXNbZnJvbV07XG5cdFx0fVxuXG5cdFx0Ly8gT3RoZXJ3aXNlLCByZXR1cm4gdGhlIGB0b2AgcmF0ZSBtdWx0aXBsZWQgYnkgdGhlIGludmVyc2Ugb2YgdGhlIGBmcm9tYCByYXRlIHRvIGdldCB0aGVcblx0XHQvLyByZWxhdGl2ZSBleGNoYW5nZSByYXRlIGJldHdlZW4gdGhlIHR3byBjdXJyZW5jaWVzXG5cdFx0cmV0dXJuIHJhdGVzW3RvXSAqICgxIC8gcmF0ZXNbZnJvbV0pO1xuXHR9O1xuXG5cblx0LyogLS0tIE9PUCB3cmFwcGVyIGFuZCBjaGFpbmluZyAtLS0gKi9cblxuXHQvLyBJZiBmeCh2YWwpIGlzIGNhbGxlZCBhcyBhIGZ1bmN0aW9uLCBpdCByZXR1cm5zIGEgd3JhcHBlZCBvYmplY3QgdGhhdCBjYW4gYmUgdXNlZCBPTy1zdHlsZVxuXHR2YXIgZnhXcmFwcGVyID0gZnVuY3Rpb24odmFsKSB7XG5cdFx0Ly8gRXhwZXJpbWVudGFsOiBwYXJzZSBzdHJpbmdzIHRvIHB1bGwgb3V0IGN1cnJlbmN5IGNvZGUgYW5kIHZhbHVlOlxuXHRcdGlmICggdHlwZW9mXHR2YWwgPT09IFwic3RyaW5nXCIgKSB7XG5cdFx0XHR0aGlzLl92ID0gcGFyc2VGbG9hdCh2YWwucmVwbGFjZSgvW14wLTktLl0vZywgXCJcIikpO1xuXHRcdFx0dGhpcy5fZnggPSB2YWwucmVwbGFjZSgvKFteQS1aYS16XSkvZywgXCJcIik7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHRoaXMuX3YgPSB2YWw7XG5cdFx0fVxuXHR9O1xuXG5cdC8vIEV4cG9zZSBgd3JhcHBlci5wcm90b3R5cGVgIGFzIGBmeC5wcm90b3R5cGVgXG5cdHZhciBmeFByb3RvID0gZngucHJvdG90eXBlID0gZnhXcmFwcGVyLnByb3RvdHlwZTtcblxuXHQvLyBmeCh2YWwpLmNvbnZlcnQob3B0cykgZG9lcyB0aGUgc2FtZSB0aGluZyBhcyBmeC5jb252ZXJ0KHZhbCwgb3B0cylcblx0ZnhQcm90by5jb252ZXJ0ID0gZnVuY3Rpb24oKSB7XG5cdFx0dmFyIGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpO1xuXHRcdGFyZ3MudW5zaGlmdCh0aGlzLl92KTtcblx0XHRyZXR1cm4gY29udmVydC5hcHBseShmeCwgYXJncyk7XG5cdH07XG5cblx0Ly8gZngodmFsKS5mcm9tKGN1cnJlbmN5KSByZXR1cm5zIGEgd3JhcHBlZCBgZnhgIHdoZXJlIHRoZSB2YWx1ZSBoYXMgYmVlbiBjb252ZXJ0ZWQgZnJvbVxuXHQvLyBgY3VycmVuY3lgIHRvIHRoZSBgZnguYmFzZWAgY3VycmVuY3kuIFNob3VsZCBiZSBmb2xsb3dlZCBieSBgLnRvKG90aGVyQ3VycmVuY3kpYFxuXHRmeFByb3RvLmZyb20gPSBmdW5jdGlvbihjdXJyZW5jeSkge1xuXHRcdHZhciB3cmFwcGVkID0gZngoY29udmVydCh0aGlzLl92LCB7ZnJvbTogY3VycmVuY3ksIHRvOiBmeC5iYXNlfSkpO1xuXHRcdHdyYXBwZWQuX2Z4ID0gZnguYmFzZTtcblx0XHRyZXR1cm4gd3JhcHBlZDtcblx0fTtcblxuXHQvLyBmeCh2YWwpLnRvKGN1cnJlbmN5KSByZXR1cm5zIHRoZSB2YWx1ZSwgY29udmVydGVkIGZyb20gYGZ4LmJhc2VgIHRvIGBjdXJyZW5jeWBcblx0ZnhQcm90by50byA9IGZ1bmN0aW9uKGN1cnJlbmN5KSB7XG5cdFx0cmV0dXJuIGNvbnZlcnQodGhpcy5fdiwge2Zyb206IHRoaXMuX2Z4ID8gdGhpcy5fZnggOiBmeC5zZXR0aW5ncy5mcm9tLCB0bzogY3VycmVuY3l9KTtcblx0fTtcblxuXG5cdC8qIC0tLSBNb2R1bGUgRGVmaW5pdGlvbiAtLS0gKi9cblxuXHQvLyBFeHBvcnQgdGhlIGZ4IG9iamVjdCBmb3IgQ29tbW9uSlMuIElmIGJlaW5nIGxvYWRlZCBhcyBhbiBBTUQgbW9kdWxlLCBkZWZpbmUgaXQgYXMgc3VjaC5cblx0Ly8gT3RoZXJ3aXNlLCBqdXN0IGFkZCBgZnhgIHRvIHRoZSBnbG9iYWwgb2JqZWN0XG5cdGlmICh0eXBlb2YgZXhwb3J0cyAhPT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRpZiAodHlwZW9mIG1vZHVsZSAhPT0gJ3VuZGVmaW5lZCcgJiYgbW9kdWxlLmV4cG9ydHMpIHtcblx0XHRcdGV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IGZ4O1xuXHRcdH1cblx0XHRleHBvcnRzLmZ4ID0gZng7XG5cdH0gZWxzZSBpZiAodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKSB7XG5cdFx0Ly8gUmV0dXJuIHRoZSBsaWJyYXJ5IGFzIGFuIEFNRCBtb2R1bGU6XG5cdFx0ZGVmaW5lKFtdLCBmdW5jdGlvbigpIHtcblx0XHRcdHJldHVybiBmeDtcblx0XHR9KTtcblx0fSBlbHNlIHtcblx0XHQvLyBVc2UgZngubm9Db25mbGljdCB0byByZXN0b3JlIGBmeGAgYmFjayB0byBpdHMgb3JpZ2luYWwgdmFsdWUgYmVmb3JlIG1vbmV5LmpzIGxvYWRlZC5cblx0XHQvLyBSZXR1cm5zIGEgcmVmZXJlbmNlIHRvIHRoZSBsaWJyYXJ5J3MgYGZ4YCBvYmplY3Q7IGUuZy4gYHZhciBtb25leSA9IGZ4Lm5vQ29uZmxpY3QoKTtgXG5cdFx0Zngubm9Db25mbGljdCA9IChmdW5jdGlvbihwcmV2aW91c0Z4KSB7XG5cdFx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XG5cdFx0XHRcdC8vIFJlc2V0IHRoZSB2YWx1ZSBvZiB0aGUgcm9vdCdzIGBmeGAgdmFyaWFibGU6XG5cdFx0XHRcdHJvb3QuZnggPSBwcmV2aW91c0Z4O1xuXHRcdFx0XHQvLyBEZWxldGUgdGhlIG5vQ29uZmxpY3QgZnVuY3Rpb246XG5cdFx0XHRcdGZ4Lm5vQ29uZmxpY3QgPSB1bmRlZmluZWQ7XG5cdFx0XHRcdC8vIFJldHVybiByZWZlcmVuY2UgdG8gdGhlIGxpYnJhcnkgdG8gcmUtYXNzaWduIGl0OlxuXHRcdFx0XHRyZXR1cm4gZng7XG5cdFx0XHR9O1xuXHRcdH0pKHJvb3QuZngpO1xuXG5cdFx0Ly8gRGVjbGFyZSBgZnhgIG9uIHRoZSByb290IChnbG9iYWwvd2luZG93KSBvYmplY3Q6XG5cdFx0cm9vdFsnZngnXSA9IGZ4O1xuXHR9XG5cblx0Ly8gUm9vdCB3aWxsIGJlIGB3aW5kb3dgIGluIGJyb3dzZXIgb3IgYGdsb2JhbGAgb24gdGhlIHNlcnZlcjpcbn0odGhpcykpO1xuIl19
