var gulp = require('gulp')
var browserify = require('browserify')
var browserSync = require('browser-sync').create()
var source = require('vinyl-source-stream')
var jade = require('gulp-jade')
var stylus = require('gulp-stylus')

gulp.task('jade', function() {
	gulp.src('./app/**/*.jade')
		.pipe(jade({
			//locals...
		})).pipe(gulp.dest('./build/'))
})

gulp.task('stylus', function() {
	gulp.src('./app/stylus/*.styl')
		.pipe(stylus())
		.pipe(gulp.dest('./build/css/'))
})

gulp.task('copy', function() {
	return gulp.src('./lib/**/*', {base: 'lib'})
		.pipe(gulp.dest('./build/js/'))
})

gulp.task('browserify', ['jade'], function() {
	return browserify({
		transform: ['coffeeify'],
		baseDir: './app/coffee/',
		entries: ['./app/coffee/main.coffee'],
		debug: true
	})
		.bundle()
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('./build/js/'))
})

gulp.task('serve', ['stylus', 'browserify'], function() {
	browserSync.init({
		server: {
			baseDir: "./build/"
		},
		port: '8001'
	})

	gulp.watch('./app/**/*.jade').on('change', function() {
		gulp.run('jade')
		browserSync.reload()
	})

	gulp.watch('./app/coffee/**/*.coffee').on('change', function() {
		gulp.run('browserify')
		browserSync.reload()
	})



})